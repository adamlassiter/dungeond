package dev.adaml.dungeond.client.ui.markdown;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.stream.Stream;

public class MarkdownFrameFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(MarkdownFrameFactory.class);
    private static final PathMatchingResourcePatternResolver RESOLVER = new PathMatchingResourcePatternResolver();

    private static final URI STYLESHEET;
    private static final String CSS_CONTENT;
    private static final String FMT_STRING = """
                        <html>
                            <head>
                                <style type="text/css">
                                    %s
                                </style>
                            </head>
                            <body>
                                <div class="phb" id="p1">
                                    %s 
                                </div>
                            </body>
                        </html>
                        """;

    static {
        try {
            STYLESHEET = new ClassPathResource("styles/phb.css").getURI();
            CSS_CONTENT = Files.readAllLines(Path.of(STYLESHEET)).stream().reduce("", String::concat);
            Stream.of(RESOLVER.getResources("classpath:fonts/**/*.otf"))
                    .map(resource -> {
                        try {
                            return Font.createFont(Font.TRUETYPE_FONT, resource.getFile());
                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .peek(font -> LOGGER.debug("Registered font {}", font.getFontName()))
                    .forEach(font -> GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font));
        } catch (IOException e) {
            throw new RuntimeException("Failed to read stylesheet", e);
        }
    }

    private final HtmlRenderer renderer;
    private final Parser parser;

    public MarkdownFrameFactory(final HtmlRenderer renderer, final Parser parser) {
        this.renderer = renderer;
        this.parser = parser;

    }

    public MarkdownFrame create(final Reader markdown) {
        return new MarkdownFrame(markdown);
    }

    public class MarkdownFrame extends JTextPane {
        private Reader markdown;

        public MarkdownFrame(final Reader markdown) throws HeadlessException {
            this.markdown = markdown;
            this.render();
        }

        public void setMarkdown(final Reader markdown) {
            this.markdown = markdown;
            this.render();
        }

        public void render() {
            try {
                Node document = parser.parseReader(this.markdown);
                String divContent = renderer.render(document);

                this.setContentType("text/html");
                this.setText(String.format(FMT_STRING, CSS_CONTENT, divContent));
            } catch (IOException e) {
                throw new RuntimeException("owo", e);
            }
        }
    }
}
