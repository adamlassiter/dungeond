package dev.adaml.dungeond.client.ui.filetree;

import com.jidesoft.swing.CheckBoxTree;
import dev.adaml.dungeond.client.ui.markdown.MarkdownFrameFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;

public class FileCollectionFrame extends CheckBoxTree {
    private static final ThreadPoolTaskExecutor EXECUTOR = new ThreadPoolTaskExecutor();
    static {
        EXECUTOR.initialize();
    }

    private final MarkdownFrameFactory.MarkdownFrame markdownFrame;
    private CompletableFuture<Void> updateMarkdown = CompletableFuture.completedFuture(null);

    public FileCollectionFrame(final Path rootPath, final MarkdownFrameFactory.MarkdownFrame markdownFrame) {
        super(new FileTreeModel(rootPath.toFile()));
        this.markdownFrame = markdownFrame;

        this.addTreeSelectionListener(e -> {
            this.updateMarkdown.cancel(true);
            this.updateMarkdown = CompletableFuture.supplyAsync(() -> {
                DisplayedFile displayedFile = (DisplayedFile) getLastSelectedPathComponent();
                try {
                    this.markdownFrame.setMarkdown(new FileReader(displayedFile));
                    this.markdownFrame.setCaretPosition(0);
                } catch (FileNotFoundException fileNotFoundException) {
                    throw new RuntimeException("File not found", fileNotFoundException);
                }
                return null;
            }, EXECUTOR);
        });
    }

}
