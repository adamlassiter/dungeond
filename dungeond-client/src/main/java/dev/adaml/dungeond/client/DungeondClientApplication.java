package dev.adaml.dungeond.client;

import dev.adaml.dungeond.client.ui.collection.MainSplitFrame;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import javax.swing.*;

@SpringBootApplication
public class DungeondClientApplication {
    private final MainSplitFrame mainSplitFrame;

    public DungeondClientApplication(final MainSplitFrame mainSplitFrame) {
        this.mainSplitFrame = mainSplitFrame;
    }

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        new SpringApplicationBuilder(DungeondClientApplication.class)
                .headless(false)
                .run(args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void postInit() {
        this.mainSplitFrame.setVisible(true);
    }

}
