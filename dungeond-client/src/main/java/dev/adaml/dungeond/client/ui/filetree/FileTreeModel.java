package dev.adaml.dungeond.client.ui.filetree;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.io.File;
import java.util.Arrays;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class FileTreeModel implements TreeModel {
    private final DisplayedFile root;

    public FileTreeModel(final File root) {
        this.root = new DisplayedFile(root);
    }

    @Override
    public void addTreeModelListener(final TreeModelListener l) {
        //do nothing
    }

    @Override
    public Object getChild(final Object parent, final int index) {
        DisplayedFile f = (DisplayedFile) parent;
        return new DisplayedFile(requireNonNull(f.listFiles())[index]);
    }

    @Override
    public int getChildCount(final Object parent) {
        DisplayedFile f = (DisplayedFile) parent;
        if (!f.isDirectory()) {
            return 0;
        } else {
            return requireNonNull(f.list()).length;
        }
    }

    @Override
    public int getIndexOfChild(final Object parent, final Object child) {
        DisplayedFile par = (DisplayedFile) parent;
        DisplayedFile ch = (DisplayedFile) child;
        return Arrays.stream(requireNonNull(par.listFiles()))
                .map(DisplayedFile::new)
                .collect(Collectors.toList())
                .indexOf(ch);
    }

    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public boolean isLeaf(final Object node) {
        DisplayedFile f = (DisplayedFile) node;
        return !f.isDirectory();
    }

    @Override
    public void removeTreeModelListener(final TreeModelListener l) {
        //do nothing
    }

    @Override
    public void valueForPathChanged(final TreePath path, final Object newValue) {
        //do nothing
    }
}
