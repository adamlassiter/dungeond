package dev.adaml.dungeond.client.ui.filetree;

import java.io.File;
import java.util.Arrays;

import static java.util.Objects.requireNonNull;

public class DisplayedFile extends File {
    public DisplayedFile(final File file) {
        super(file.getPath());
    }

    @Override
    public File[] listFiles() {
        return Arrays.stream(requireNonNull(super.listFiles()))
                .sorted(new DirectoryFirstFileComparator())
                .toArray(File[]::new);
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
