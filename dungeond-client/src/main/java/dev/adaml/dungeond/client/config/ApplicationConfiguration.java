package dev.adaml.dungeond.client.config;

import dev.adaml.dungeond.client.ui.collection.MainSplitFrame;
import dev.adaml.dungeond.client.ui.filetree.FileCollectionFrame;
import dev.adaml.dungeond.client.ui.markdown.MarkdownFrameFactory;
import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.Reader;
import java.nio.file.Path;
import java.util.List;

@Configuration
public class ApplicationConfiguration {

    @Bean("rootPath")
    Path rootPath() {
        return Path.of(System.getProperty("user.dir"));
    }

    @Bean
    MainSplitFrame mainFrame(final @Qualifier("rootPath") Path rootPath, final MarkdownFrameFactory markdownFrameFactory) {
        MarkdownFrameFactory.MarkdownFrame markdownFrame = markdownFrameFactory.create(Reader.nullReader());
        FileCollectionFrame fileCollectionFrame = new FileCollectionFrame(rootPath, markdownFrame);
        return new MainSplitFrame(fileCollectionFrame, markdownFrame);
    }

    @Bean
    MarkdownFrameFactory markdownFrameFactory(final HtmlRenderer renderer, final Parser parser) {
        return new MarkdownFrameFactory(renderer, parser);
    }

    @Bean
    HtmlRenderer htmlRenderer() {
        return HtmlRenderer.builder()
                .extensions(List.of(TablesExtension.create()))
                .build();
    }

    @Bean
    Parser parser() {
        return Parser.builder()
                .extensions(List.of(TablesExtension.create()))
                .build();
    }
}
