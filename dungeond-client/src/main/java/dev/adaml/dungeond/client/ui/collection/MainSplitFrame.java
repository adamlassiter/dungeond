package dev.adaml.dungeond.client.ui.collection;

import dev.adaml.dungeond.client.ui.filetree.FileCollectionFrame;
import dev.adaml.dungeond.client.ui.markdown.MarkdownFrameFactory;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.WindowConstants;
import java.awt.Frame;
import java.awt.HeadlessException;

public class MainSplitFrame extends JFrame {
    private final FileCollectionFrame fileCollectionFrame;
    private final MarkdownFrameFactory.MarkdownFrame markdownFrame;

    public MainSplitFrame(final FileCollectionFrame fileCollectionFrame, final MarkdownFrameFactory.MarkdownFrame markdownFrame) throws HeadlessException {
        this.fileCollectionFrame = fileCollectionFrame;
        this.markdownFrame = markdownFrame;
        this.init();
    }

    public void init() {
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setState(Frame.NORMAL);
        this.setSize(800, 400);

        JScrollPane fileScrollPane = new JScrollPane(this.fileCollectionFrame);
        JScrollPane markdownScrollPane = new JScrollPane(this.markdownFrame, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, fileScrollPane, markdownScrollPane);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(200);

        this.add(splitPane);
    }
}
