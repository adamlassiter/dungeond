package dev.adaml.dungeond.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * While REST /api endpoints return {@link java.util.Optional}, {@link java.util.stream.Stream} or {@link java.util.List}
 * monadic results, HTML model controllers may prefer to throw exceptions for bad requests.
 */
@ControllerAdvice
public class ExceptionHandlerAdvice {
    @ExceptionHandler(ControllerException.class)
    public ResponseEntity<?> handleException(ControllerException e) {
        return ResponseEntity
                .status(e.getHttpStatus())
                .build();
    }

    private abstract static class ControllerException extends RuntimeException {
        abstract HttpStatus getHttpStatus();
    }

    public static class ForbiddenException extends ControllerException {
        @Override
        HttpStatus getHttpStatus() {
            return HttpStatus.FORBIDDEN;
        }
    }
}
