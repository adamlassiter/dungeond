package dev.adaml.dungeond.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.stream.Stream;

@RestController
@RequestMapping("/meta")
public class ApiMetaRestController {
    @Autowired
    private BuildProperties buildProperties;
    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;

    @GetMapping("/api")
    public Stream<RequestMappingInfo> getApiEndpoints() {
        return requestMappingHandlerMapping.getHandlerMethods()
                .keySet().stream();
    }

    @GetMapping("/build")
    public BuildProperties getBuildProperties() {
        return buildProperties;
    }
}
