package dev.adaml.dungeond.web;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

@Configuration
public class WebControllerConfiguration {
    private static final String X_CLACKS_OVERHEAD = "X-Clacks-Overhead";

    @Bean
    public FilterRegistrationBean<Filter> loggingFilter(@Qualifier(X_CLACKS_OVERHEAD) Filter xClacksOverhead) {
        return new FilterRegistrationBean<>(xClacksOverhead);
    }

    @Bean(X_CLACKS_OVERHEAD)
    public Filter xClacksOverhead() {
        return (ServletRequest req, ServletResponse res, FilterChain chain) -> {
            HttpServletResponse response = (HttpServletResponse) res;
            response.setHeader(X_CLACKS_OVERHEAD, "GNU Terry Pratchett");
            chain.doFilter(req, res);
        };
    }
}
