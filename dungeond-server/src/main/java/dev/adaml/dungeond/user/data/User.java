package dev.adaml.dungeond.user.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class User {
    private String name;
    private String password;
    private Boolean gameMaster;
}
