package dev.adaml.dungeond.user.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties("data")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserProperties {
    private List<User> users;
}

