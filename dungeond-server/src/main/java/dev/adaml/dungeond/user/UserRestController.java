package dev.adaml.dungeond.user;

import dev.adaml.dungeond.user.data.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/user")
public class UserRestController {
    @Autowired
    private UserService userService;

    @GetMapping
    public Mono<User> getUser(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return userService.getUser(userDetails);
    }

    @GetMapping("/all")
    public Flux<String> getUsernames(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return userService.getUsernamesForGameMaster(userDetails);
    }
}
