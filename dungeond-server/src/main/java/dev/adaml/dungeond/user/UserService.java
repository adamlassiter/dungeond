package dev.adaml.dungeond.user;

import dev.adaml.dungeond.user.data.User;
import dev.adaml.dungeond.user.persistence.UserPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;

@Service
public class UserService implements UserDetailsService {
    public static final GrantedAuthority GAME_MASTER = new SimpleGrantedAuthority("GM");
    public static final GrantedAuthority PLAYER = new SimpleGrantedAuthority("PLAYER");
    public static final GrantedAuthority USER = new SimpleGrantedAuthority("USER");

    @Autowired
    private UserPersistence userPersistence;

    public Mono<User> getUser(UserDetails userDetails) {
        return userPersistence.getUser(userDetails.getUsername());
    }

    public Flux<String> getUsernamesForGameMaster(UserDetails userDetails) {
        return getUser(userDetails)
                .filter(User::getGameMaster).flux()
                .flatMap(ignored -> userPersistence.getUsernames());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userPersistence.getUser(username)
                .blockOptional()
                .orElseThrow(() -> new UsernameNotFoundException("Username '" + username + "' not found"));
        Set<GrantedAuthority> authorities = Set.of(USER, user.getGameMaster() ? GAME_MASTER : PLAYER);
        return new org.springframework.security.core.userdetails.User(user.getName(), user.getPassword(), authorities);
    }
}
