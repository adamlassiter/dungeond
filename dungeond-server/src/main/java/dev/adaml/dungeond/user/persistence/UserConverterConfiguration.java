package dev.adaml.dungeond.user.persistence;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.adaml.dungeond.user.data.User;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.stereotype.Component;

@Configuration
public class UserConverterConfiguration {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @WritingConverter
    @Component
    static class UserToJsonConverter implements Converter<User, String> {
        @SneakyThrows(JsonProcessingException.class)
        @Override
        public String convert(User source) {
            return MAPPER.writeValueAsString(source);
        }
    }

    @ReadingConverter
    @Component
    static class JsonToUserConverter implements Converter<String, User> {
        @SneakyThrows(JsonProcessingException.class)
        @Override
        public User convert(String source) {
            return MAPPER.readValue(source, User.class);
        }
    }
}
