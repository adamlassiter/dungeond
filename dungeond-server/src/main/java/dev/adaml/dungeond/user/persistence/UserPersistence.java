package dev.adaml.dungeond.user.persistence;

import dev.adaml.dungeond.user.data.User;
import dev.adaml.dungeond.user.data.UserProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class UserPersistence {
    @Autowired
    private UserRepository repository;
    @Autowired
    private UserProperties properties;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Mono<User> getUser(String userId) {
        return repository.findById(userId)
                .map(UserEntity::getUser);
    }

    public Flux<String> getUsernames() {
        return repository.findAll()
                .map(UserEntity::getUser)
                .map(User::getName);
    }

    @EventListener(ApplicationReadyEvent.class)
    void prepopulatePersistence() {
        List<UserEntity> entities = properties.getUsers()
                .stream()
                .peek(user -> user.setPassword(passwordEncoder.encode(user.getPassword())))
                .map(user -> new UserEntity(user.getName(), user))
                .collect(Collectors.toList());
        repository.saveAll(entities)
                .reduce(log, (logger, entity) -> {
                    logger.info("Populated users with '{}'", entity.getName());
                    logger.debug("User object was '{}'", entity.getUser());
                    return logger;
                }).block();
    }
}
