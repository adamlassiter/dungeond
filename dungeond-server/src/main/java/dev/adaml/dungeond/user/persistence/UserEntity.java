package dev.adaml.dungeond.user.persistence;

import dev.adaml.dungeond.user.data.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor(onConstructor = @__({@PersistenceConstructor}))
@NoArgsConstructor
@Getter
@Table("users")
public class UserEntity implements Persistable<String> {
    @Id
    @Column("name")
    private String name;
    @Column("user")
    private User user;

    @Override
    public String getId() {
        return name;
    }

    @Override
    public boolean isNew() {
        return true;
    }
}
