package dev.adaml.dungeond.character.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class MiscBlock {
    private String classLevel;
    private String background;
    private String race;
    private String alignment;
    private Integer experience;
}
