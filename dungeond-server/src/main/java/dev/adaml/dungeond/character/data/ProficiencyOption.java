package dev.adaml.dungeond.character.data;

public enum ProficiencyOption {
    NONE(" "),
    PROFICIENT("✓"),
    EXPERT("★");

    private final String displayValue;

    ProficiencyOption(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
