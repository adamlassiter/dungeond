package dev.adaml.dungeond.character.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Character {
    private String name;
    private String user;
    private MiscBlock misc;
    private AttributesBlock attributes;
    private SkillsBlock skills;
    private CombatBlock combat;
}
