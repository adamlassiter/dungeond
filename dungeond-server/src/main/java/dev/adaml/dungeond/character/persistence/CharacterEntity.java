package dev.adaml.dungeond.character.persistence;

import dev.adaml.dungeond.character.data.Character;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor(onConstructor = @__({@PersistenceConstructor}))
@NoArgsConstructor
@Getter
@Table("characters")
public class CharacterEntity implements Persistable<String> {
    @Id
    @Column("name")
    private String name;
    @Column("user")
    private String user;
    @Column("character")
    private Character character;

    @Override
    public String getId() {
        return name;
    }

    @Override
    public boolean isNew() {
        return true;
    }
}
