package dev.adaml.dungeond.character;

import dev.adaml.dungeond.character.data.Character;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/character")
public class CharacterRestController {
    @Autowired
    private CharacterService characterService;

    @GetMapping
    public Flux<Character> getCharacters(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return characterService.getCharacters(userDetails);
    }

    @GetMapping("/{characterName}")
    public Mono<Character> getCharacter(
            Authentication authentication,
            @PathVariable("characterName") String characterName) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return characterService.getCharacter(userDetails, characterName);
    }
}
