package dev.adaml.dungeond.character.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Optional;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class AttributesBlock {
    private Integer strength;
    private Integer dexterity;
    private Integer constitution;
    private Integer intelligence;
    private Integer wisdom;
    private Integer charisma;

    private Integer proficiency;
    private Boolean strengthProf;
    private Boolean dexterityProf;
    private Boolean constitutionProf;
    private Boolean intelligenceProf;
    private Boolean wisdomProf;
    private Boolean charismaProf;

    private static int mod(Integer attribute) {
        int attr = Optional.ofNullable(attribute).orElse(10);
        return (Math.abs(attr) / 2) - 5;
    }

    private int save(Integer attribute, Boolean isProficient) {
        int save = Optional.ofNullable(isProficient).map(ign -> proficiency).orElse(0);
        return mod(attribute) + save;
    }

    @JsonIgnore
    public Integer getProficiencyMod() {
        return proficiency;
    }

    @JsonIgnore
    public Integer getStrengthMod() {
        return mod(strength);
    }
    @JsonIgnore
    public Integer getDexterityMod() {
        return mod(dexterity);
    }
    @JsonIgnore
    public Integer getConstitutionMod() {
        return mod(constitution);
    }
    @JsonIgnore
    public Integer getIntelligenceMod() {
        return mod(intelligence);
    }
    @JsonIgnore
    public Integer getWisdomMod() {
        return mod(wisdom);
    }
    @JsonIgnore
    public Integer getCharismaMod() {
        return mod(charisma);
    }

    @JsonIgnore
    public Integer getStrengthSave() {
        return save(strength, strengthProf);
    }
    @JsonIgnore
    public Integer getDexteritySave() {
        return save(dexterity, dexterityProf);
    }
    @JsonIgnore
    public Integer getConstitutionSave() {
        return save(constitution, constitutionProf);
    }
    @JsonIgnore
    public Integer getIntelligenceSave() {
        return save(intelligence, intelligenceProf);
    }
    @JsonIgnore
    public Integer getWisdomSave() {
        return save(wisdom, wisdomProf);
    }
    @JsonIgnore
    public Integer getCharismaSave() {
        return save(charisma, charismaProf);
    }
}
