package dev.adaml.dungeond.character.persistence;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.adaml.dungeond.character.data.Character;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.stereotype.Component;

@Configuration
public class CharacterConverterConfiguration {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @WritingConverter
    @Component
    static class CharacterToJsonConverter implements Converter<Character, String> {
        @SneakyThrows(JsonProcessingException.class)
        @Override
        public String convert(Character source) {
            return MAPPER.writeValueAsString(source);
        }
    }

    @ReadingConverter
    @Component
    static class JsonToCharacterConverter implements Converter<String, Character> {
        @SneakyThrows(JsonProcessingException.class)
        @Override
        public Character convert(String source) {
            return MAPPER.readValue(source, Character.class);
        }
    }
}
