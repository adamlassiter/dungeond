package dev.adaml.dungeond.character.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SkillsBlock {
    private ProficiencyOption acrobaticsProf = ProficiencyOption.NONE;
    private ProficiencyOption animalHandlingProf = ProficiencyOption.NONE;
    private ProficiencyOption arcanaProf = ProficiencyOption.NONE;
    private ProficiencyOption athleticsProf = ProficiencyOption.NONE;
    private ProficiencyOption deceptionProf = ProficiencyOption.NONE;
    private ProficiencyOption historyProf = ProficiencyOption.NONE;
    private ProficiencyOption insightProf = ProficiencyOption.NONE;
    private ProficiencyOption intimidationProf = ProficiencyOption.NONE;
    private ProficiencyOption investigationProf = ProficiencyOption.NONE;
    private ProficiencyOption medicineProf = ProficiencyOption.NONE;
    private ProficiencyOption natureProf = ProficiencyOption.NONE;
    private ProficiencyOption perceptionProf = ProficiencyOption.NONE;
    private ProficiencyOption performanceProf = ProficiencyOption.NONE;
    private ProficiencyOption persuasionProf = ProficiencyOption.NONE;
    private ProficiencyOption religionProf = ProficiencyOption.NONE;
    private ProficiencyOption sleightOfHandProf = ProficiencyOption.NONE;
    private ProficiencyOption survivalProf = ProficiencyOption.NONE;
    private ProficiencyOption stealthProf = ProficiencyOption.NONE;

    private Integer acrobatics;
    private Integer animalHandling;
    private Integer arcana;
    private Integer athletics;
    private Integer deception;
    private Integer history;
    private Integer insight;
    private Integer intimidation;
    private Integer investigation;
    private Integer medicine;
    private Integer nature;
    private Integer perception;
    private Integer performance;
    private Integer persuasion;
    private Integer religion;
    private Integer sleightOfHand;
    private Integer survival;
    private Integer stealth;
}
