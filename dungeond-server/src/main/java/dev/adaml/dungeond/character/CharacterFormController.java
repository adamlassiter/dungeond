package dev.adaml.dungeond.character;

import dev.adaml.dungeond.character.data.Character;
import dev.adaml.dungeond.web.ExceptionHandlerAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;
import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/form/character")
public class CharacterFormController {
    private static final String MODEL_KEY = "character";

    @Autowired
    private CharacterService characterService;

    @GetMapping(value = "/{characterName}", produces = MediaType.TEXT_HTML_VALUE)
    public String getCharacterForm(
            Authentication authentication,
            @PathVariable("characterName") String characterName,
            Model model) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        characterService.getCharacter(userDetails, characterName)
                .map(character -> model.addAttribute(MODEL_KEY, character))
                .switchIfEmpty(Mono.error(ExceptionHandlerAdvice.ForbiddenException::new))
                .block();
        return MODEL_KEY;
    }

    @PostMapping(value = "/{characterName}")
    public View postCharacterForm(
            Authentication authentication,
            @PathVariable("characterName") String characterName,
            @ModelAttribute Character character) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return new RedirectView(character.getName(), true);
    }
}
