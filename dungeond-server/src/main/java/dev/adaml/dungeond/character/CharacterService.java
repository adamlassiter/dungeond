package dev.adaml.dungeond.character;

import dev.adaml.dungeond.character.data.Character;
import dev.adaml.dungeond.character.persistence.CharacterPersistence;
import dev.adaml.dungeond.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CharacterService {
    @Autowired
    private CharacterPersistence characterPersistence;
    @Autowired
    private UserService userService;

    public Flux<Character> getCharacters(UserDetails userDetails) {
        return userService.getUser(userDetails).flux()
                .flatMap(characterPersistence::getCharacters);
    }

    public Mono<Character> getCharacter(UserDetails userDetails, String characterName) {
        return userService.getUser(userDetails)
                .flatMap(user -> characterPersistence.getCharacter(characterName)
                        .filter(character -> character.getUser().equals(user.getName())));
    }
}
