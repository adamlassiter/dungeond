package dev.adaml.dungeond.character.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CombatBlock {
    private Integer ac;
    private Integer initiative;
    private Integer speed;
    private Integer passivePerception;
    private Integer maxHealth;
    private Integer currentHealth;
    private Integer tempHealth;
}
