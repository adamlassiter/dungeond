package dev.adaml.dungeond.character.persistence;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface CharacterRepository extends ReactiveCrudRepository<CharacterEntity, String> {
    Flux<CharacterEntity> findAllByUser(String user);
}
