package dev.adaml.dungeond.character.persistence;

import dev.adaml.dungeond.character.data.Character;
import dev.adaml.dungeond.character.data.CharacterProperties;
import dev.adaml.dungeond.user.data.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class CharacterPersistence {
    @Autowired
    private CharacterRepository repository;
    @Autowired
    private CharacterProperties properties;

    public Mono<Character> getCharacter(String characterId) {
        return repository.findById(characterId)
                .map(CharacterEntity::getCharacter);
    }

    public Flux<Character> getCharacters(User user) {
        return repository.findAllByUser(user.getName())
                .map(CharacterEntity::getCharacter);
    }

    @EventListener(ApplicationReadyEvent.class)
    void prepopulatePersistence() {
        Flux.fromStream(properties.getCharacters()::stream)
                .map(character -> new CharacterEntity(character.getName(), character.getUser(), character))
                .flatMap(repository::save)
                .reduce(log, (logger, entity) -> {
                    logger.info("Populated characters with '{}'", entity.getName());
                    logger.debug("Character object was '{}'", entity.getCharacter());
                    return logger;
                }).block();
    }
}
