package dev.adaml.dungeond.character.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties("data")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class CharacterProperties {
    private List<Character> characters = List.of();
}
