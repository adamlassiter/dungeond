package dev.adaml.dungeond.lore.util;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.Renderer;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class IOProcessor {
    private final Parser parser;
    private final Reader reader;

    private final Renderer renderer;
    private final Writer writer;

    public IOProcessor(Parser parser, Reader reader, Renderer renderer, Writer writer) {
        this.parser = parser;
        this.reader = reader;
        this.renderer = renderer;
        this.writer = writer;
    }

    public void process() {
        try {
            Node document = parser.parseReader(reader);
            renderer.render(document, writer);
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static IParser create() {
        return parser -> reader -> renderer -> writer ->
                new IOProcessor(parser, reader, renderer, writer);
    }

    public interface IParser {
        IReader parser(Parser parser);
    }

    public interface IReader {
        IRenderer reader(Reader reader);
    }

    public interface IRenderer {
        IWriter renderer(Renderer renderer);
    }

    public interface IWriter {
        IOProcessor writer(Writer writer);
    }
}
