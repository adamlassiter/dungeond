package dev.adaml.dungeond.lore.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Lore {
    private Path directory;
    private List<Path> allow;

    public Set<Path> getAllow() {
        return allow.stream()
                .map(directory::resolve)
                .collect(Collectors.toSet());
    }
}
