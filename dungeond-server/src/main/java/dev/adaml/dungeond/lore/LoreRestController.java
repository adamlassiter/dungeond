package dev.adaml.dungeond.lore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.servlet.http.HttpServletRequest;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.nio.file.Path;

@RestController
@RequestMapping(LoreRestController.REST_PARENT_PATH)
public class LoreRestController {
    protected static final String REST_PARENT_PATH = "/api/lore/";

    @Autowired
    private LoreService loreService;

    @GetMapping
    public Flux<URI> getLorePages(Authentication authentication, HttpServletRequest request) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return loreService.getLorePages(userDetails)
                .map(path -> join(request, path));
    }

    @GetMapping(value = "/**")
    public Mono<ResponseEntity<StreamingResponseBody>> getLorePage(
            Authentication authentication,
            HttpServletRequest request) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        Path requestPath = split(request);

        return loreService.getLorePage(userDetails, requestPath)
                .map(ioProcessor -> (StreamingResponseBody) outputStream ->
                        ioProcessor.writer(new OutputStreamWriter(outputStream)).process())
                .map(body -> ResponseEntity.ok()
                        .contentType(MediaType.TEXT_HTML)
                        .body(body))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    private static URI join(HttpServletRequest request, Path relativePath) {
        String baseUri = request.getRequestURL()
                .toString()
                .replaceFirst(request.getRequestURI(), request.getContextPath());
        return URI.create(baseUri)
                .resolve(REST_PARENT_PATH)
                .resolve(relativePath.toUri());
    }

    private static Path split(HttpServletRequest request) {
        String subPath = request.getRequestURI().split(REST_PARENT_PATH)[1];
        return Path.of(subPath);
    }
}
