package dev.adaml.dungeond.lore;

import dev.adaml.dungeond.lore.data.LoreProperties;
import dev.adaml.dungeond.lore.util.IOProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.file.Path;

@Service
public class MarkdownHtmlLoreService implements LoreService {
    @Autowired
    private LoreProperties loreProperties;
    @Autowired
    private MarkdownProcessor markdownProcessor;

    public Flux<Path> getLorePages(UserDetails userDetails) {
        return getLorePaths(userDetails, loreProperties.getLore(), Path.of("."));
    }

    public Mono<IOProcessor.IWriter> getLorePage(UserDetails userDetails, Path lorePage) {
        return this.getLorePaths(userDetails, loreProperties.getLore(), lorePage)
                .filter(path -> path.endsWith(lorePage)).singleOrEmpty()
                .map(markdownProcessor::getRenderer);
    }
}
