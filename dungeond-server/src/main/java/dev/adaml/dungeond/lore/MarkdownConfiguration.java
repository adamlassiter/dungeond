package dev.adaml.dungeond.lore;

import org.commonmark.ext.gfm.tables.TablesExtension;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class MarkdownConfiguration {
    @Bean
    HtmlRenderer htmlRenderer() {
        return HtmlRenderer.builder()
                .extensions(List.of(TablesExtension.create()))
                .build();
    }

    @Bean
    Parser parser() {
        return Parser.builder()
                .extensions(List.of(TablesExtension.create()))
                .build();
    }
}
