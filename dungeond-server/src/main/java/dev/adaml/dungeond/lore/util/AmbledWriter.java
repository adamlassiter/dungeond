package dev.adaml.dungeond.lore.util;

import java.io.IOException;
import java.io.Writer;

public class AmbledWriter extends Writer {
    private final Writer delegate;
    private final String postamble;

    public AmbledWriter(Writer delegate, String preamble, String postamble) throws IOException {
        this.delegate = delegate;
        this.postamble = postamble;

        this.delegate.write(preamble);
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        this.delegate.write(cbuf, off, len);
    }

    @Override
    public void flush() throws IOException {
        this.delegate.flush();
    }

    @Override
    public void close() throws IOException {
        this.delegate.write(postamble);
        this.delegate.close();
    }
}
