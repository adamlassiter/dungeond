package dev.adaml.dungeond.lore;

import dev.adaml.dungeond.lore.util.AmbledWriter;
import dev.adaml.dungeond.lore.util.IOProcessor;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;

@Service
public class MarkdownProcessor {
    @Autowired
    private Parser parser;
    @Autowired
    private HtmlRenderer renderer;

    public IOProcessor.IWriter getRenderer(Path lorePage) {
        return writer -> IOProcessor.create()
                .parser(parser)
                .reader(createFileReader(lorePage))
                .renderer(renderer)
                .writer(createAmbledWriter(writer));
    }

    private static final String PREAMBLE = """
            <html>
                <head>
                    <link rel="stylesheet" href="/styles/phb.css"/>
                </head>
                <body>
                    <div class="phb" id="p1">
            """;
    private static final String POSTAMBLE = """
                    </div>
                </body>
            </html>
            """;

    private static FileReader createFileReader(Path path) {
        try {
            return new FileReader(path.toFile());
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Failed to construct FileReader", e);
        }
    }

    private static AmbledWriter createAmbledWriter(Writer writer) {
        try {
            return new AmbledWriter(writer, PREAMBLE, POSTAMBLE);
        } catch (IOException e) {
            throw new IllegalArgumentException("Failed to construct FileReader", e);
        }
    }
}
