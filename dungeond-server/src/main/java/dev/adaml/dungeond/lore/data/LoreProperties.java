package dev.adaml.dungeond.lore.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("data")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class LoreProperties {
    private Lore lore;
}
