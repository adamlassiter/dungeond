package dev.adaml.dungeond.lore;

import dev.adaml.dungeond.lore.data.Lore;
import dev.adaml.dungeond.lore.util.IOProcessor;
import dev.adaml.dungeond.user.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.function.Predicate;

public interface LoreService {
    Flux<Path> getLorePages(UserDetails userDetails);

    Mono<IOProcessor.IWriter> getLorePage(UserDetails userDetails, Path lorePage);

    default Flux<Path> getLorePaths(UserDetails userDetails, Lore lore, Path subPath) {
        Path fullPath = lore.getDirectory().resolve(subPath);
        Set<Path> allowedSet = lore.getAllow();

        Predicate<Path> allowedPred = allowedSet::contains;
        Predicate<Path> gameMasterPred = ign -> userDetails.getAuthorities()
                .contains(UserService.GAME_MASTER);
        try {
            return Flux.fromStream(Files.walk(fullPath))
                    .filter(allowedPred.or(gameMasterPred));
        } catch (IOException ex) {
            return Flux.empty();
        }
    }
}
