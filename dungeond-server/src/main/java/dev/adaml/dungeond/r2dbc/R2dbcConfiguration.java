package dev.adaml.dungeond.r2dbc;

import io.r2dbc.spi.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.convert.R2dbcCustomConversions;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.r2dbc.connection.init.CompositeDatabasePopulator;
import org.springframework.r2dbc.connection.init.ConnectionFactoryInitializer;
import org.springframework.r2dbc.connection.init.ResourceDatabasePopulator;

import java.util.Collection;
import java.util.stream.Stream;

@Slf4j
@Configuration
@EnableR2dbcRepositories(basePackages = {"dav.adaml.dungeond"})
public class R2dbcConfiguration extends AbstractR2dbcConfiguration {
    @Autowired
    private Collection<Converter<?, ?>> converters;
    private final ConnectionFactory defaultConnectionFactory;

    public R2dbcConfiguration(final ConnectionFactory defaultConnectionFactory) {
        this.defaultConnectionFactory = defaultConnectionFactory;
    }

    @Bean
    ConnectionFactoryInitializer initializer(ConnectionFactory connectionFactory) {
        ConnectionFactoryInitializer initializer = new ConnectionFactoryInitializer();
        initializer.setConnectionFactory(connectionFactory);

        CompositeDatabasePopulator populator = new CompositeDatabasePopulator();
        Stream.of(new ClassPathResource("sql/schema.sql"), new ClassPathResource("sql/data.sql"))
                .filter(ClassPathResource::exists)
                .peek(resource -> log.info("Found populator resource: {}", resource))
                .forEach(resource -> populator.addPopulators(new ResourceDatabasePopulator(resource)));
        initializer.setDatabasePopulator(populator);

        return initializer;
    }

    @Override
    public ConnectionFactory connectionFactory() {
        return this.defaultConnectionFactory;
    }

    @Bean
    @Override
    public R2dbcCustomConversions r2dbcCustomConversions() {
        log.info("Registered {} custom converters", converters.size());
        converters.forEach(converter -> log.debug("Registered custom converter: {}", converter.getClass()));
        return new R2dbcCustomConversions(getStoreConversions(), converters);
    }
}
