package dev.adaml.dungeond;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class DungeondServerApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(DungeondServerApplication.class)
                .run(args);
    }
}
